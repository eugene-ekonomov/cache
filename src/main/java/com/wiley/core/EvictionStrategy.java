package com.wiley.core;

public enum EvictionStrategy {
    LRU, LFU
}
