package com.wiley.core;

public interface Cache {
    Object get(int key);

    void put(int key, Object value);
}
