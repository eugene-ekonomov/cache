package com.wiley.core;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

class LFUCache implements Cache {
    private Map<Integer, Node> values;
    private Map<Integer, Set<Integer>> freqs;
    private int minFreq = 1;
    private int capacity;
    private int count = 0;
    LFUCache(int capacity) {
        values = new HashMap<>();
        freqs = new HashMap<>();
        this.capacity = capacity;
        freqs.put(1, new LinkedHashSet<>());
    }

    public Object get(int key) {
        if (!values.containsKey(key)) {
            return -1;
        }
        Node n = values.get(key);
        freqs.get(n.freq).remove(key);
        n.freq++;
        if (!freqs.containsKey(n.freq)) {
            freqs.put(n.freq, new LinkedHashSet<>());
        }
        freqs.get(n.freq).add(key);
        if (n.freq - 1 == minFreq && freqs.get(n.freq - 1).size() == 0) {
            minFreq = n.freq;
        }
        return n.val;
    }

    public void put(int key, Object value) {
        if (capacity == 0) {
            return;
        }
        if (values.containsKey(key)) {
            values.get(key).val = value;
            get(key);
            return;
        }
        if (count == capacity) {
            count--;
            int minKey = freqs.get(minFreq).iterator().next();
            freqs.get(minFreq).remove(minKey);
            values.remove(minKey);
        }
        Node n = new Node(key, value);
        values.put(key, n);
        minFreq = 1;
        freqs.get(1).add(key);
        count++;
    }
    private static class Node {
        int key, freq;
        Object val;
        Node (int k, Object v) {
            key = k;
            val = v;
            freq = 1;
        }
    }

}
