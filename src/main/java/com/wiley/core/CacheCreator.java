package com.wiley.core;

public class CacheCreator {
    public static Cache getCache(int capacity, EvictionStrategy evictionStrategy){
        if(evictionStrategy==EvictionStrategy.LRU){
            return new LRUCache(capacity);
        }else if(evictionStrategy==EvictionStrategy.LFU){
            return new LFUCache(capacity);
        }else{
            return null;
        }
    }

}
