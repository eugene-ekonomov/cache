package com.wiley.core;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

class LRUCache implements Cache {
    private HashMap<Integer, Object> data;

    LRUCache(int capacity) {
        data = new CacheMap<>(capacity);
    }

    public Object get(int key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            data.remove(key);
            data.put(key, value);
            return value;
        }
        return -1;
    }

    public void put(int key, Object value) {
        if (data.containsKey(key)) {
            data.remove(key);
        }
        data.put(key, value);
    }

    private static class CacheMap<K, V> extends LinkedHashMap<K, V> {
        int capacity;

        CacheMap(int capacity) {
            this.capacity = capacity;
        }

        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > capacity;
        }
    }
}