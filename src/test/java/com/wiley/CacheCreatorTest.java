package com.wiley;

import com.wiley.core.Cache;
import com.wiley.core.CacheCreator;
import com.wiley.core.EvictionStrategy;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CacheCreatorTest {

    @Test
    public void cacheLRU() {
        Cache cacheLRU = CacheCreator.getCache(2, EvictionStrategy.LRU);

        cacheLRU.put(1, 1); //1
        cacheLRU.put(2, 2); //1, 2
        Assert.assertEquals(1, cacheLRU.get(1));       // 1, 2
        cacheLRU.put(3, 3);    // 2, 3
        cacheLRU.put(4, 4);    // 3, 4
        cacheLRU.put(5, "test");    // 4, 5
        Assert.assertEquals(-1, cacheLRU.get(1));
        Assert.assertEquals("test", cacheLRU.get(5));
        Assert.assertEquals(-1, cacheLRU.get(2));
    }

    @Test
    public void cacheLFU() {
        Cache cacheLFU = CacheCreator.getCache(2, EvictionStrategy.LFU);

        cacheLFU.put(1, 1); //1
        cacheLFU.put(2, 2); //1, 2
        Assert.assertEquals(1, cacheLFU.get(1));       // 1, 2
        cacheLFU.put(3, 3);    // 1, 3
        cacheLFU.put(4, 4);    // 1, 4
        cacheLFU.put(5, "test");    // 1, 5
        Assert.assertEquals(1, cacheLFU.get(1));
        Assert.assertEquals("test", cacheLFU.get(5));
        Assert.assertEquals(-1, cacheLFU.get(2));

    }
}